package main

// #cgo CFLAGS: -g -Wall
// #include <stdlib.h>
// #include "greeter.h"
import "C"
import (
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"unsafe"
)

type Payload struct {
	Stuff Data
}
type Data struct {
	Body   Boards
	Status Def
}
type Boards map[string]string
type Def map[string]int

func YourHandler(w http.ResponseWriter, r *http.Request) {

	boards := make(map[string]string)

	def := make(map[string]int)
	def["status"] = 200

	d := Data{boards, def}
	p := Payload{d}

	name := C.CString("Gopher")
	defer C.free(unsafe.Pointer(name))

	year := C.int(2018)

	ptr := C.malloc(C.sizeof_char * 1024)
	defer C.free(unsafe.Pointer(ptr))

	size := C.greet(name, year, (*C.char)(ptr))

	b := C.GoBytes(ptr, size)
	w.Header().Set("Content-Type", "application/json")
	boards["message"] = string(b)
	response, err := json.Marshal(p)
	if err != nil {
	}
	_, _ = w.Write(response)
}

func main() {
	r := mux.NewRouter()

	// Routes consist of a path and a handler function.
	r.HandleFunc("/", YourHandler)

	// Bind to a port and pass our router in
	log.Fatal(http.ListenAndServe(":8000", r))
}
